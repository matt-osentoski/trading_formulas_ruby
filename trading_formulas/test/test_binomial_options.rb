require 'test/unit'
require 'trading_formulas'

class BinomialOptionsTest < Test::Unit::TestCase
  
  def test_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.call(s, k, r, sigma, time, steps)
    assert_equal(14.9505, test_val.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.call(s, k, r, sigma, time, steps)
    assert_not_equal(14.9509, test_val.round(4))
  end
  
  def test_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.put(s, k, r, sigma, time, steps)
    assert_equal(6.54691, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.put(s, k, r, sigma, time, steps)
    assert_not_equal(6.54699, test_val.round(5))
  end
  
  def test_delta_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.delta_call(s, k, r, sigma, 
                                                         time, steps)
    assert_equal(0.699792, test_val.round(6))                                               
  end
  
  ##
  # Negative test case
  def test_invalid_delta_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.delta_call(s, k, r, sigma, 
                                                         time, steps)
    assert_not_equal(0.699799, test_val.round(6))                                               
  end
  
  def test_delta_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.delta_put(s, k, r, sigma, 
                                                            time, steps)
    assert_equal(-0.387636, test_val.round(6))
  end
  
  ##
  # Negative test case
  def test_invalid_delta_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.delta_put(s, k, r, sigma, 
                                                            time, steps)
    assert_not_equal(-0.387639, test_val.round(6))
  end
  
  def test_partials_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    delta, gamma, theta, vega, rho = TradingFormulas::BinomialOptions.partials_call(s, k, r, sigma, time, steps)
    assert_equal(0.699792, delta.round(6))
    assert_equal(0.0140407, gamma.round(7))
    assert_equal(-9.89067, theta.round(5))
    assert_equal(34.8536, vega.round(4))
    assert_equal(56.9652, rho.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_partials_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    delta, gamma, theta, vega, rho = TradingFormulas::BinomialOptions.partials_call(s, k, r, sigma, time, steps)
    assert_not_equal(0.699799, delta.round(6))
    assert_not_equal(0.0140409, gamma.round(7))
    assert_not_equal(-9.89069, theta.round(5))
    assert_not_equal(34.8539, vega.round(4))
    assert_not_equal(56.9659, rho.round(4))
  end
  
  def test_partials_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    delta, gamma, theta, vega, rho = TradingFormulas::BinomialOptions.partials_put(s, k, r, sigma, time, steps)
    assert_equal(-0.387636, delta.round(6))
    assert_equal(0.0209086, gamma.round(7))
    assert_equal(-1.99027, theta.round(5))
    assert_equal(35.3943, vega.round(4))
    assert_equal(-21.5433, rho.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_partials_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    delta, gamma, theta, vega, rho = TradingFormulas::BinomialOptions.partials_put(s, k, r, sigma, time, steps)
    assert_not_equal(-0.387639, delta.round(6))
    assert_not_equal(0.0209089, gamma.round(7))
    assert_not_equal(-1.99029, theta.round(5))
    assert_not_equal(35.3949, vega.round(4))
    assert_not_equal(-21.5439, rho.round(4))
  end
  
  def test_discrete_dividends_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BinomialOptions.discrete_dividends_call(s, k, 
      r, sigma, time, steps, dividend_times, dividend_amounts)
    assert_equal(12.0233, test_val.round(4))     
  end
  
  ##
  # Negative test case
  def test_invalid_discrete_dividends_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BinomialOptions.discrete_dividends_call(s, k, 
      r, sigma, time, steps, dividend_times, dividend_amounts)
    assert_not_equal(12.0239, test_val.round(4))    
  end
  
  def test_discrete_dividends_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BinomialOptions.discrete_dividends_put(s, k, 
                r, sigma, time, steps, dividend_times, dividend_amounts)
    assert_equal(8.11801, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_discrete_dividends_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BinomialOptions.discrete_dividends_put(s, k, 
                r, sigma, time, steps, dividend_times, dividend_amounts)
    assert_not_equal(8.11809, test_val.round(5))
  end
  
  def test_proportional_dividends_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_yields = [0.025, 0.025]
    test_val = TradingFormulas::BinomialOptions.proportional_dividends_call(s, 
            k, r, sigma, time, steps, dividend_times, dividend_yields)
    assert_equal(11.8604, test_val.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_proportional_dividends_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_yields = [0.025, 0.025]
    test_val = TradingFormulas::BinomialOptions.proportional_dividends_call(s, 
            k, r, sigma, time, steps, dividend_times, dividend_yields)
    assert_not_equal(11.8609, test_val.round(4))
  end
  
  def test_proportional_dividends_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_yields = [0.025, 0.025]
    test_val = TradingFormulas::BinomialOptions.proportional_dividends_put(s, 
            k, r, sigma, time, steps, dividend_times, dividend_yields)
    assert_equal(7.99971, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_proportional_dividends_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    dividend_times = [0.25, 0.75]
    dividend_yields = [0.025, 0.025]
    test_val = TradingFormulas::BinomialOptions.proportional_dividends_put(s, 
            k, r, sigma, time, steps, dividend_times, dividend_yields)
    assert_not_equal(7.99979, test_val.round(5))
  end
  
  def test_continuous_payout_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    y = 0.02
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.continuous_payout_call(s, k, r, y, 
                                                    sigma, time, steps)
    assert_equal(13.5926, test_val.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_continuous_payout_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    y = 0.02
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.continuous_payout_call(s, k, r, y, 
                                                    sigma, time, steps)
    assert_not_equal(13.5929, test_val.round(4))
  end
  
  def test_continuous_payout_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    y = 0.02
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.continuous_payout_put(s, k, r, y, 
                                                    sigma, time, steps)
    assert_equal(6.99407, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_continuous_payout_put
        s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    y = 0.02
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.continuous_payout_put(s, k, r, y, 
                                                    sigma, time, steps)
    assert_not_equal(6.99409, test_val.round(5))
  end
  
  def test_european_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.european_call(s, k, r, sigma, time, steps)
    assert_equal(14.9505, test_val.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_european_call
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.european_call(s, k, r, sigma, time, steps)
    assert_not_equal(14.9509, test_val.round(4))
  end
  
  def test_european_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.european_put(s, k, r, sigma, time, steps)
    assert_equal(5.43425, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_european_put
    s = 100
    k = 100
    r = 0.10
    sigma = 0.25
    time = 1.0
    steps = 100
    test_val = TradingFormulas::BinomialOptions.european_put(s, k, r, sigma, time, steps)
    assert_not_equal(5.43429, test_val.round(5))
  end
end