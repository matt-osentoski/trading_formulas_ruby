require 'test/unit'
require 'trading_formulas'

class BlackScholesTest < Test::Unit::TestCase
  def test_n
    test_val = TradingFormulas::BlackScholes.n(1.23)
    assert_equal(0.187235, test_val.round(6))
  end
  
  ##
  # Negative test case
  def test_invalid_n
    test_val = TradingFormulas::BlackScholes.n(1.23)
    assert_not_equal(0.187239, test_val.round(6))
  end
  
  def test_N
    test_val = TradingFormulas::BlackScholes.N(1.23)
    assert_equal(0.890651, test_val.round(6))
  end
  
  ##
  # Negative test case
  def test_invalid_N
    test_val = TradingFormulas::BlackScholes.N(1.23)
    assert_not_equal(0.890659, test_val.round(6))
  end
  
  def test_call
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.call(s, k, r, sigma, time)
    assert_equal(5.45325, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_call
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.call(s, k, r, sigma, time)
    assert_not_equal(5.45329, test_val.round(5))
  end
  
  def test_put
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.put(s, k, r, sigma, time)
    assert_equal(3.01472, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_put
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.put(s, k, r, sigma, time)
    assert_not_equal(3.01479, test_val.round(5))
  end
  
  def test_delta_call
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.delta_call(s, k, r, sigma, time)
    assert_equal(0.633737, test_val.round(6))
  end
  
  ##
  # Negative test case
  def test_invalid_delta_call
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.delta_call(s, k, r, sigma, time)
    assert_not_equal(0.633739, test_val.round(6))
  end
  
  def test_delta_put
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.delta_put(s, k, r, sigma, time)
    assert_equal(-0.366263, test_val.round(6))
  end
  
  ##
  # Negative test case
  def test_invalid_delta_put
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    test_val = TradingFormulas::BlackScholes.delta_put(s, k, r, sigma, time)
    assert_not_equal(-0.366269, test_val.round(6))
  end
  
  def test_implied_volatility_call_bisections
    s = 50
    k = 50
    r = 0.10
    time = 0.50
    option_price = 2.5
    test_val = TradingFormulas::BlackScholes.implied_volatility_call_bisections(
                                                s, k, r, time, option_price)
    assert_equal(0.0500419, test_val.round(7))
  end
  
  ##
  # Negative test case
  def test_invalid_implied_volatility_call_bisections
    s = 50
    k = 50
    r = 0.10
    time = 0.50
    option_price = 2.5
    test_val = TradingFormulas::BlackScholes.implied_volatility_call_bisections(
                                                s, k, r, time, option_price)
    assert_not_equal(0.0500411, test_val.round(7))
  end
  
  def test_implied_volatility_call_newton
    s = 50
    k = 50
    r = 0.10
    time = 0.50
    option_price = 2.5
    test_val = TradingFormulas::BlackScholes.implied_volatility_call_newton(s, 
                                                k, r, time, option_price)
    assert_equal(0.0500427, test_val.round(7))                                           
  end
  
  ##
  # Negative test case
  def test_invalid_implied_volatility_call_newton
    s = 50
    k = 50
    r = 0.10
    time = 0.50
    option_price = 2.5
    test_val = TradingFormulas::BlackScholes.implied_volatility_call_newton(s, 
                                                k, r, time, option_price)
    assert_not_equal(0.0500429, test_val.round(7))                                           
  end
  
  def test_partials_call
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    delta, gamma, theta, vega, rho = TradingFormulas::BlackScholes.partials_call(s, k, r, sigma, time)
    assert_equal(0.633737, delta.round(6))  
    assert_equal(0.0354789, gamma.round(7))  
    assert_equal(-6.61473, theta.round(5))
    assert_equal(13.3046, vega.round(4))
    assert_equal(13.1168, rho.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_partials_call
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    delta, gamma, theta, vega, rho = TradingFormulas::BlackScholes.partials_call(s, k, r, sigma, time)
    assert_not_equal(0.633739, delta.round(6))  
    assert_not_equal(0.0354781, gamma.round(7))  
    assert_not_equal(-6.61479, theta.round(5))
    assert_not_equal(13.3049, vega.round(4))
    assert_not_equal(13.1169, rho.round(4))
  end
  
  def test_partials_put
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    delta, gamma, theta, vega, rho = TradingFormulas::BlackScholes.partials_put(s, k, r, sigma, time)
    assert_equal(-0.366263, delta.round(6))  
    assert_equal(0.0354789, gamma.round(7))  
    assert_equal(-1.85859, theta.round(5))
    assert_equal(13.3046, vega.round(4))
    assert_equal(-10.6639, rho.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_partials_put
    s = 50
    k = 50
    r = 0.10
    sigma = 0.30
    time = 0.50
    delta, gamma, theta, vega, rho = TradingFormulas::BlackScholes.partials_put(s, k, r, sigma, time)
    assert_not_equal(-0.366269, delta.round(6))  
    assert_not_equal(0.0354781, gamma.round(7))  
    assert_not_equal(-1.85851, theta.round(5))
    assert_not_equal(13.3049, vega.round(4))
    assert_not_equal(-10.6631, rho.round(4))
  end
  
  def test_european_call_payout
    s = 100
    k = 100
    r = 0.1
    q = 0.05
    sigma = 0.25
    time = 1.0
    test_val = TradingFormulas::BlackScholes.european_call_payout(s, k, r, q, sigma, time)
    assert_equal(11.7344, test_val.round(4))
  end
  
  ## 
  # Negative test case
  def test_invalid_european_call_payout
    s = 100
    k = 100
    r = 0.1
    q = 0.05
    sigma = 0.25
    time = 1.0
    test_val = TradingFormulas::BlackScholes.european_call_payout(s, k, r, q, sigma, time)
    assert_not_equal(11.7349, test_val.round(4))
  end
  
  def test_european_put_payout
    s = 100
    k = 100
    r = 0.1
    q = 0.05
    sigma = 0.25
    time = 1.0
    test_val = TradingFormulas::BlackScholes.european_put_payout(s, k, r, q, sigma, time)
    assert_equal(7.09515, test_val.round(5))
  end
  
  ## 
  # Negative test case
  def test_invalid_european_put_payout
    s = 100
    k = 100
    r = 0.1
    q = 0.05
    sigma = 0.25
    time = 1.0
    test_val = TradingFormulas::BlackScholes.european_put_payout(s, k, r, q, sigma, time)
    assert_not_equal(7.09519, test_val.round(5))
  end
  
  def test_european_call_dividends
    s = 100
    k = 100
    r = 0.1
    sigma = 0.25
    time = 1.0
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BlackScholes.european_call_dividends(s, k, r, sigma, 
                    time, dividend_times, dividend_amounts )
    assert_equal(11.8094, test_val.round(4))
  end
  
  ## 
  # Negative test case
  def test_inactive_european_call_dividends
    s = 100
    k = 100
    r = 0.1
    sigma = 0.25
    time = 1.0
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BlackScholes.european_call_dividends(s, k, r, sigma, 
                    time, dividend_times, dividend_amounts )
    assert_not_equal(11.8099, test_val.round(4))
  end
  
  def test_european_put_dividends
    s = 100
    k = 100
    r = 0.1
    sigma = 0.25
    time = 1.0
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BlackScholes.european_put_dividends(s, k, r, sigma, 
                    time, dividend_times, dividend_amounts )
    assert_equal(7.05077, test_val.round(5))
  end
  
  ## 
  # Negative test case
  def test_invalid_european_put_dividends
    s = 100
    k = 100
    r = 0.1
    sigma = 0.25
    time = 1.0
    dividend_times = [0.25, 0.75]
    dividend_amounts = [2.5, 2.5]
    test_val = TradingFormulas::BlackScholes.european_put_dividends(s, k, r, sigma, 
                    time, dividend_times, dividend_amounts )
    assert_not_equal(7.05079, test_val.round(5))
  end
  
end
