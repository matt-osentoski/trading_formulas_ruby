require 'test/unit'
require 'trading_formulas'

class BermudanOptionsTest < Test::Unit::TestCase
  
  def test_call
    s = 80
    k = 100
    r = 0.20
    q = 0.0
    sigma = 0.25
    time = 1.0
    steps = 500
    potential_exercise_times = [0.25, 0.5, 0.75]
    test_val = TradingFormulas::BermudanOptions.call(s, k, r, q, sigma, time, 
                                    potential_exercise_times, steps)
    assert_equal(7.14016, test_val.round(5))
  end
  
  ##
  # Negative test case
  def test_invalid_call
    s = 80
    k = 100
    r = 0.20
    q = 0.0
    sigma = 0.25
    time = 1.0
    steps = 500
    potential_exercise_times = [0.25, 0.5, 0.75]
    test_val = TradingFormulas::BermudanOptions.call(s, k, r, q, sigma, time, 
                                    potential_exercise_times, steps)
    assert_not_equal(7.14019, test_val.round(5))
  end
  
  def test_put
    s = 80
    k = 100
    r = 0.20
    q = 0.0
    sigma = 0.25
    time = 1.0
    steps = 500
    potential_exercise_times = [0.25, 0.5, 0.75]
    test_val = TradingFormulas::BermudanOptions.put(s, k, r, q, sigma, time, 
                                    potential_exercise_times, steps)
    assert_equal(15.8869, test_val.round(4))
  end
  
  ##
  # Negative test case
  def test_invalid_put
    s = 80
    k = 100
    r = 0.20
    q = 0.0
    sigma = 0.25
    time = 1.0
    steps = 500
    potential_exercise_times = [0.25, 0.5, 0.75]
    test_val = TradingFormulas::BermudanOptions.put(s, k, r, q, sigma, time, 
                                    potential_exercise_times, steps)
    assert_not_equal(15.8861, test_val.round(4))
  end
end