# -*- encoding: utf-8 -*-
require File.expand_path('../lib/trading_formulas/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Matt Osentoski"]
  gem.email         = ["matt.osentoski@gmail.com"]
  gem.description   = %q{Technical Analysis and Option trading formulas for financial instruments}
  gem.summary       = %q{This Gem contains a number of trading formulas.}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "trading_formulas"
  gem.require_paths = ["lib"]

  gem.version       = TradingFormulas::VERSION
end
