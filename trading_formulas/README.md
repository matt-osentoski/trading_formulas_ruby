# TradingFormulas

This gem contains trading formulas for Technical Analysis and Derivatives

## Installation

Add this line to your application's Gemfile:

    gem 'trading_formulas'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install trading_formulas

## Usage

s = 50
k = 50
r = 0.10
sigma = 0.30
time = 0.50
option_price = TradingFormulas::BlackScholes.call(s, k, r, sigma, time)

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
